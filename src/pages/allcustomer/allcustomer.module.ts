import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AllcustomerPage } from './allcustomer';

@NgModule({
  declarations: [
    AllcustomerPage,
  ],
  imports: [
    IonicPageModule.forChild(AllcustomerPage),
  ],
})
export class AllcustomerPageModule {}
