import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Api} from '../../providers/api/api';
import { Item } from '../../models/item';
import { Items } from '../../providers';

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {

  currentItems: any = [];
  userid:any;
  allcustomer:any;
  constructor(public api: Api,public navCtrl: NavController, public navParams: NavParams, public items: Items) { 

  }

  /**
   * Perform a service for the proper items.
   */
  getItems(ev) {
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.currentItems = [];
      return;
    }
    this.currentItems = this.items.query({
      name: val
    });
  }

  ionViewWillEnter(){
    console.log("all customer")
    this.userid=0;
    let seq = this.api.get('applogin/allcustomer', this.userid).share();
    seq.map(resp => resp.json())
        .subscribe((resp) => {   
           console.log('resp',resp);
            if (resp.status == 'success') {
              this.allcustomer = resp.pendingcustomer;
                 // loader.dismiss();
               
            } else { 
               
            }
        }, (err) => {
            console.error('ERROR', err);
           
        });
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Item) {
    this.navCtrl.push('ItemDetailPage', {
      item: item
    });
  }

}
