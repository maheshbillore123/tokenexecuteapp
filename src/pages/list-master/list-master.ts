import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController } from 'ionic-angular';
import {Api} from '../../providers/api/api';
import { Item } from '../../models/item';
import { Items } from '../../providers';
import {TokenexecutePage} from '../tokenexecute/tokenexecute';

@IonicPage()
@Component({
  selector: 'page-list-master',
  templateUrl: 'list-master.html'
})
export class ListMasterPage {
  currentItems: any;
  userid:any;
  pendingcustomer:any;
  constructor(public api: Api,public navCtrl: NavController, public modalCtrl: ModalController) {
    // this.currentItems = this.items.query();
  }

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
  }
 
  ionViewWillEnter(){
    this.userid=0;
    let seq = this.api.get('applogin/todaypending', this.userid).share();
    seq.map(resp => resp.json())
        .subscribe((resp) => {   
           console.log('resp',resp);
            if (resp.status == 'success') {
              this.pendingcustomer = resp.pendingcustomer;
                 // loader.dismiss();
               
            } else { 
               
            }
        }, (err) => {
            console.error('ERROR', err);
           
        });
  }
  /**
   * Prompt the user to add a new item. This shows our ItemCreatePage in a
   * modal and then adds the new item to our data source if the user created one.
   */
  

  /**
   * Delete an item from the list of items.
   */
 

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Item) {
    this.navCtrl.push('ItemDetailPage', {
      item: item
    });
  }

  showdetails(item:Item){
    console.log("maehsh",item);
    this.navCtrl.push(TokenexecutePage,{
      item:item
    });
    // this.navCtrl.setRoot(TokenexecutePage);
  }
}
