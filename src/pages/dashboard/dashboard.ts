import { Component } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { IonicPage, NavController, ToastController ,NavParams,LoadingController } from 'ionic-angular';
import {Api} from '../../providers/api/api';
import {Camera, CameraOptions} from '@ionic-native/camera';
import {Network} from '@ionic-native/network';
import {TodayallcustomerPage} from '../todayallcustomer/todayallcustomer';
import {TodaydonecustomerPage} from '../todaydonecustomer/todaydonecustomer';
import {TodaypendingcustomerPage} from '../todaypendingcustomer/todaypendingcustomer';
import {AllcustomerPage} from '../allcustomer/allcustomer';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
	  form: FormGroup;
    isReadyToSave: boolean;
    public base64Image: string;
    imagestatus:any;
    tokennum:any;
  constructor(formBuilder: FormBuilder,public camera: Camera, formBuildr: FormBuilder,  public network: Network,public navCtrl: NavController, public api: Api,public loadingCtrl: LoadingController, public navParams: NavParams, public toastCtrl: ToastController, ) {
   this.imagestatus=false;

  	this.form = formBuilder.group({
            phone: ['', Validators.compose([Validators.maxLength(10),
            Validators.minLength(10), Validators.pattern('[0-9 ]*'), Validators.required])],
            name: ['',Validators.required],
            description :[''],
            image: [''],
 
        });
        this.form.valueChanges.subscribe((v) => { 
            this.isReadyToSave = this.form.valid;
        });  

         this.tokennum = '';
    }
  

 
  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }


 networkAlert() {
        let toast = this.toastCtrl.create({
            message: 'Please check your internet connection!',
            duration: 3000,
            position: 'top'
        });
        toast.present();
    }

submit(){

    if (this.network.type == 'none') {
            this.networkAlert();
            return false;
        }
    this.getPictureFromCamera();

}

getPictureFromCamera() {
     // this.camera.Direction.FRONT = 1;

    const options: CameraOptions = {
        quality: 50, // picture quality
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
       correctOrientation: true,
      cameraDirection: 1
    }
    this.camera.getPicture(options).then((imageData) => {
        this.form.patchValue({'image': 'data:image/jpg;base64,' + imageData});
        this.base64Image = 'data:image/jpg;base64,' + imageData;
        
         console.log("image1",this.base64Image);
       this.form.value.image = this.base64Image;
         if (!this.form.valid) {return;}
         console.log(this.form.value);
         let loader = this.loadingCtrl.create({
             content: "Please wait..."
         });
    
         loader.present();
         let seq = this.api.post('applogin/submit', this.form.value).share();
         seq.map(resp => resp.json())
             .subscribe((resp) => {  
  
                 if (resp.status == 'success') {

                     loader.dismiss();  
                     this.navCtrl.setRoot(DashboardPage);
                     this.tokennum = resp.tokennumber;
                      // loader.dismiss();
                     let toast = this.toastCtrl.create({
                         message: 'Token Generated Successfully ,Your Token Number is, #'+this.tokennum ,
                         duration: 4000,
                         position: 'middle',
                         cssClass: 'yourtoastclass'
                     }); 
                     toast.present();
 
                 } else { 
                     loader.dismiss();
                     let toast = this.toastCtrl.create({
                         message: 'Something Went Wrong ,Try Again',
                         duration: 3000,
                         position: 'top',
                     });
                     toast.present();
                 }
 
 
             }, (err) => {
                 loader.dismiss();
                 console.error('ERROR', err);
                 loader.dismiss();
 
             });
 



         
    }, (err) => {
        console.log("blank1");
       
        console.log(err);
    });
}

todaytotalcustomer(){

    this.navCtrl.setRoot(TodayallcustomerPage);
    // console.log("today total customer");
}
todaypendingcustomer(){
    console.log("today pending customer");
    this.navCtrl.setRoot(AllcustomerPage);
}
todaydonecustomer(){
    this.navCtrl.setRoot(TodaydonecustomerPage);
    // console.log("today Done customer");
}
totalcustomer(){
    this.navCtrl.setRoot(TodaypendingcustomerPage);
    console.log(" total customer");
}

}
