import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TodaydonecustomerPage } from './todaydonecustomer';

@NgModule({
  declarations: [
    TodaydonecustomerPage,
  ],
  imports: [
    IonicPageModule.forChild(TodaydonecustomerPage),
  ],
})
export class TodaydonecustomerPageModule {}
