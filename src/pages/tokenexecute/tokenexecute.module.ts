import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TokenexecutePage } from './tokenexecute';

@NgModule({
  declarations: [
    TokenexecutePage,
  ],
  imports: [
    IonicPageModule.forChild(TokenexecutePage),
  ],
})
export class TokenexecutePageModule {}
