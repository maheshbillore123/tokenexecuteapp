import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { TabsPage } from '../tabs/tabs';
import {Api} from '../../providers/api/api';
/**
 * Generated class for the TokenexecutePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tokenexecute',
  templateUrl: 'tokenexecute.html',
})
export class TokenexecutePage {
  item: any;
  form: FormGroup;
  itemvalue:any;

  constructor(public api:Api ,public navCtrl: NavController, public navParams: NavParams, formBuilder: FormBuilder) {
 
    this.item = navParams.get('item');
console.log('token exucte',this.item);

this.form = formBuilder.group({
  customername: [this.item.name,Validators.compose([Validators.maxLength(150) ,Validators.required])],
  mobile: [this.item.contactNo,Validators.compose([Validators.maxLength(150) ,Validators.required])],
  tokennumber: [this.item.TokenNumber,Validators.compose([Validators.maxLength(150) ,Validators.required])],
  description: ['',Validators.compose([Validators.required])],
  image:[''] 
});

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TokenexecutePage');
  }
  submit(){
    console.log("form value",this.form.value);
    // this.navCtrl.push(ListMasterPage); 
    let itemvalue = {
      "id": this.item.id,
      "description": this.form.value.description,
  };
 
    let seq = this.api.post('applogin/execute',itemvalue).share();
    seq.map(resp => resp.json())
        .subscribe((resp) => {   
           console.log('resp',resp);
            if (resp.status == 'success') {
              // this.pendingcustomer = resp.pendingcustomer;
                 // loader.dismiss();
                 this.navCtrl.setRoot(TabsPage);
               
            } else { 
               
            }
        }, (err) => {
            console.error('ERROR', err);
           
        });
   
  }

}
