import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

import { User } from '../../providers';  
import { MainPage } from '../';
import { TabsPage } from '../tabs/tabs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
   form: FormGroup;
    isReadyToSave: boolean;
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { email: string, password: string } = {
    email: '',
    password: '' 
  };

  // Our translated text strings
  private loginErrorString: string;
  
  constructor(public navCtrl: NavController,
    formBuilder: FormBuilder, formBuildr: FormBuilder,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService) {

    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })

    this.form = formBuilder.group({
           
            email: ['',Validators.required],
            password: ['',Validators.required],
 
        });
        this.form.valueChanges.subscribe((v) => { 
            this.isReadyToSave = this.form.valid;
        }); 
  }

  // Attempt to login in through our User service
  doLogin() {
      if (!this.form.valid) {return;}   
        this.user.login(this.account).subscribe((resp) => {

          // console.log(this.user._user); 
            if (this.user._user) {
               this.navCtrl.setRoot(TabsPage);
                let toast = this.toastCtrl.create({
                    message: 'You LoggedIn Successfully',
                    duration: 3000,
                    position: 'bottom'
                });
                toast.present(); 
            } else {
                // Unable to log in
                let toast = this.toastCtrl.create({
                    message: this.loginErrorString,
                    duration: 3000,
                    position: 'top'
                });
                toast.present();
            }
        }, (err) => {
            // Unable to log
            let toast = this.toastCtrl.create({
                message: this.loginErrorString,
                duration: 3000,
                position: 'top'
            });
            toast.present();
        });
}

}