import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TodayallcustomerPage } from './todayallcustomer';

@NgModule({
  declarations: [
    TodayallcustomerPage,
  ],
  imports: [
    IonicPageModule.forChild(TodayallcustomerPage),
  ],
})
export class TodayallcustomerPageModule {}
