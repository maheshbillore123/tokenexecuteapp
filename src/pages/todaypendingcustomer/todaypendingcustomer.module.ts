import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TodaypendingcustomerPage } from './todaypendingcustomer';

@NgModule({
  declarations: [
    TodaypendingcustomerPage,
  ],
  imports: [
    IonicPageModule.forChild(TodaypendingcustomerPage),
  ],
})
export class TodaypendingcustomerPageModule {}
