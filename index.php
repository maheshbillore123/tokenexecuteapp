<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\User;
use backend\models\ImportantLocations;
use yii\widgets\Pjax;

$impLoca = ImportantLocations::find()->select('il_id,il_name,il_image')->where(['il_status' => 'ACTIVE'])->asArray()->all();
$allloca = array();

foreach ($impLoca as $key => $value) {
    $allloca[$value['il_id']] = $value;
}
$alloca = json_encode($allloca);

$this->title = 'Important Locations Details';
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
.load_bg{
    width: 100%;
    height: 100%;
    position:fixed;
    z-index:9999;
    background: rgba(0,0,0,0.25);
    margin-top: -60px;
}
    #load img{
        top: 40%;
        position: absolute;
        left: 40%;
        transform: translate(-50%,-50%);
        width: 10%;
        border-radius: 10px;
    }
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCvUoPsFeYvaHSTwWs4cOL2WxG2S9bTREg&libraries=drawing" async defer></script>
<script src="<?= Url::base() ?>/compress/jsxcompressor.min.js" type="text/javascript"></script>  
<div class="importantlocationsdetails-index">
    <div id="load" style="display: none;">
        <div class="load_bg">
            <img src="<?= Url::base() ?>/resource/img/tenor1.gif">
        </div>
    </div>
    <div id="contents">
        <section class="content-header">
            <h1 class="cbreadcrum fornt1"><?= Html::encode($this->title) ?>
                <?= Html::a(Yii::t('app', ' <i class=\'ion-ios-plus-outline\'></i>'), ['create'], ['class' => 'fa-lg']) ?>
                <button class="btn btn-default" onclick="exportdata()"><i class="fa fa-download" aria-hidden="true"></i> Export</button>
                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#ImportModal"><i class="fa fa-upload" aria-hidden="true"></i> Import</button>


            </h1>
            <?=
            Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ])
            ?>
        </section>
    </div>

    <div class="col-md-12" style="padding-top: 15px;">
        <?php if (Yii::$app->session->hasFlash('error')): ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="icon fa fa-check"></i> 
                        <?= Yii::$app->session->getFlash('error') ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?php if (Yii::$app->session->hasFlash('success')): ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <i class="icon fa fa-check"></i> 
                        <?= Yii::$app->session->getFlash('success') ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    </div>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary table-responsive">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <?php echo $this->render('_search', ['model' => $searchModel, 'imploc' => $impLoca]);
                                ?>  

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-8">

                                <div id="map" style="height: 480px;">

                                </div>  
                                <div id="infowindow-content">
                                    <span id="place-name"  class="title"></span><br>
                                    <span id="place-address"></span>

                                </div>

                            </div>

                            <div class="col-xs-4">
                                <?php
                                Pjax::begin([
                                    'timeout' => false,
                                    'enablePushState' => false,
                                    'clientOptions' => ['method' => 'POST']]);
                                ?>

                                <?=
                                GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    // 'filterModel' => $searchModel,
                                    'columns' => [
                                        [
                                            'class' => 'yii\grid\CheckboxColumn',
                                            'checkboxOptions' => function($model) {
                                                return ['value' => $model->ild_id, 'onclick' => 'selectedrouts(this)', 'id' => 'srouts' . $model->ild_id, 'name' => 'srouts'];
                                            },
                                            'header' => Html::checkbox('selection_all', false, ['class' => 'select-on-check-all', 'value' => 1, 'onclick' => 'selectedrouts(this,true)']),
                                        ],
//                                        'id',
                                        ['attribute' => 'Important Location',
                                            'format' => 'raw',
                                            'value' => function($model) {
                                                $url = Url::base() . "/importantlocationsdetails/view?id=" . $model->ild_id;
                                                return '<a href="' . $url . '">' . $model->ild_name . '</a>';
                                            }
                                        ],
                                    //       ['attribute' => 'Location Type',
                                    //         'format' => 'raw',
                                    //         'value' => function($model) {
                                    // $data = Importantlocations::findone($model->ild_il_id)->il_name;
                                    //   return $data;
                                    //     },
                                    //     ],
                                    //          ['attribute' => 'Created BY',
                                    //     'format' => 'raw',
                                    //     'filter'=>false,
                                    //     'value' => function($model) {
                                    // $data = user::findone($model->ild_createdBy)->name;
                                    //   return $data;
                                    //     },
                                    // ],
                                    ],
                                ]);
                                ?>
                                <?php Pjax::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="distanceSign" style="display:none;">
            <div style="width:120px; height:30px">
                <span>Radius :</span><span class="radiusValue"></span><span>km</span>
            </div>
        </div>
    </section></div>
</div>
<div class="modal fade" id="ImportModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Import Data</h4>
            </div>

            <?php $form = ActiveForm::begin(['action' => ['importantlocationsdetails/import'], 'id' => 'upload', 'options' => ['enctype' => 'multipart/form-data']]); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        <div class="form-group">
                            <a href="<?= Url::toRoute(['importantlocationsdetails/exporttemplate']); ?>">Download template</a>
                        </div>

                        <div class="form-group">
                            <label class="control-label" >Import File</label>
                            <input type="file" id="fileToUpload" class="form-control" name="excelfile" aria-required="true">
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger" >Submit</button>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<script src="<?= Url::base() ?>/resource/mapjs/markerclusterer.js"></script>
<!--<script src="<?= Url::base() ?>/resource/mapjs/oms.min.js"></script>-->


<script>

                    var markers = [];
                    var selectedShape;
                    var infoWindow;
                    var currentOverlay;
                    var markerCluster = null;
                    var mycircle;
                    var allrouts = [];
                    var allcheck = true;
                    var alluncheck = false;
                    var bounds;
                    var requestArray = [], renderArray = [];
                    var bermudaArray = [];
                    var oms;
                    $(document).ready(function () {
                        $(".hidetable").hide();

                        initMap();



                    });

                    function initMap() {
//                    var mapOptions = {
//                        center: new google.maps.LatLng(23.2599, 77.4126),
//                        zoom: 9,
//                        mapTypeId: google.maps.MapTypeId.ROADMAP
//                    };
//                    map = new google.maps.Map(document.getElementById("map"), mapOptions);
                        map = new google.maps.Map(document.getElementById('map'), {
                            center: new google.maps.LatLng(23.2599, 77.4126),
                            zoom: 8,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        });
                      //  selectedrouts('initial load', true);
                        setDrawingTool();
                        loadGoogleMaps();
                    }

                    function selectedrouts(val, all = false, type = '', rid = '', circleclick = '') {
                        // alert("maehshh");
                        $("#alluncheck").val('');
                        $("#load").show();
                        clearOverlays();
                        var test = false;
                        requestArray = [];
                        markers = [];
                        drawArray = '';
                        if (rid == '') {
                            if (!all) {
                                allcheck = false;
                                var ischecked = $('#' + val.id).is(':checked');
                                if (ischecked) {
                                    allrouts.push(val.value);
                                } else {
                                    var index = allrouts.indexOf(val.value);
                                    if (index > -1) {
                                        allrouts.splice(index, 1);
                                    }
                                }
                                console.log("allrouts pallavi", allrouts);
                            } else if (val == '' || val == 'initial load') {
                                allcheck = true;
                                var mydata = '<?php echo implode(",", $ild_id); ?>';
                                // console.log("mydata",mydata);
                                if (mydata) {
                                    allrouts = mydata.split(",");
                                }
                                $('input:checkbox').prop('checked', true);
                            } else {
                                if (val.checked) {
                                    allcheck = true;
                                    var mydata = '<?php echo implode(",", $ild_id); ?>';
                                    allrouts = mydata.split(",");

                                } else {
                                    allcheck = false;
                                    $("#alluncheck").val('set');
                                    allrouts = [];
                                    test = true;
                                }
                            }
                        }
                        var id = id;
                        var datanew = [];
                        $('#loadingmessage').show();
                        $('#map').css('opacity', '0.4');

                        var infoWindow = new google.maps.InfoWindow();
                        if (!test) {
                            $("#importantlocationsdetailssearch-ild_id").val(allrouts.join(","));
                            $.ajax({
                                url: "<?= Yii::$app->urlManager->createUrl('importantlocationsdetails/getlocationinfo'); ?>",
                                type: "post",
                                beforeSend: function () {
                                    $('#image').show();
                                     $("#load").show();
                                },
                                data: $('#w0').serializeArray(),
                                success: function (res) {
                                   
                                    if (markerCluster) {
                                        markerCluster.clearMarkers();
                                    }
                                    var data = JXG.decompress(res);
                                    var mydata = JSON.parse(data);
                                    var ImpLoc = JSON.parse('<?= $alloca ?>');
                                    var latlong = mydata;
                                    if (latlong.length > 0) {
                                        var rendererOptions = {
                                            map: map,
                                            // suppressMarkers: true
                                        };
                                        var mapdata1 = [];
                                        bounds = new google.maps.LatLngBounds();
                                        oms = new OverlappingMarkerSpiderfier(map,
                                                {
                                                    markersWontMove: true,
                                                    markersWontHide: false,
                                                    keepSpiderfied: true
                                                });
                                        var spider = baseUrl + "images/pointer.png";
                                        var spidermarker = baseUrl + "images/mark.png";
                                        var usualColor = 'eebb22';
                                        var spiderfiedColor = 'ffee22';
                                        var iconWithColor = function (color)
                                        {
                                            return 'http://chart.googleapis.com/chart?chst=d_map_xpin_letter&chld=pin|+|' +
                                                    color + '|000000|ffff00';
                                        }

                                        var shadow = new google.maps.MarkerImage(
                                                'https://www.google.com/intl/en_ALL/mapfiles/shadow50.png',
                                                new google.maps.Size(37, 34), // size   - for sprite clipping
                                                new google.maps.Point(0, 0), // origin - ditto
                                                new google.maps.Point(10, 34)  // anchor - where to meet map location
                                                );
                                        console.log(latlong, 'latlong')
                                        markers = latlong.map(function (location, i) {
                                            var ltlong = location.ild_location;
                                            var name = location.ild_name;
                                            var ltlong1 = JSON.parse(ltlong);

                                            var lat = ltlong1[0].lat;
                                            var lng = ltlong1[0].lng;
                                            var index = ImpLoc[location.ild_il_id];
                                            var imgpath = baseUrl + 'resource/img/markers/place.png';
                                            var icon;
                                            if (index && index.il_image) {
                                                imgpath = baseUrl + index.il_image;
                                                icon = {
                                                    url: imgpath,
                                                    scaledSize: new google.maps.Size(40, 60),
                                                };
                                            } else {
                                                imgpath = baseUrl + 'resource/img/markers/place.png'
                                                console.log(imgpath, 'new')
                                                icon = {
                                                    url: imgpath, // url
                                                    scaledSize: new google.maps.Size(40, 40),
                                                };
                                            }
                                            marker = new google.maps.Marker({
                                                position: new google.maps.LatLng(lat, lng), //location,
                                                animation: google.maps.Animation.DROP,
                                                draggable: true,
                                                title: name,
                                                id: location.ild_id,
                                                icon: icon,
                                            });
                                            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                                                return function () {
                                                    infoWindow.setContent(location.ild_name);
                                                    infoWindow.open(map, marker);
                                                }
                                            })(marker, i));

                                            bounds.extend(marker.getPosition());
                                            oms.addMarker(marker);
                                            return marker;
                                        });

                                        var clusterStyles = [
                                            {
                                                height: 42,
                                                width: 42
                                            }
                                        ];

                                        markerCluster = new MarkerClusterer(map, markers, {ac: clusterStyles, gridSize: 50, maxZoom: 15, ignoreHidden: true, imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
                                        map.fitBounds(bounds);

                                    } else {
                                        map.fitBounds(bounds);
                                    }
                                    $('#loadingmessage').hide();
                                    $('#map').css('opacity', '1');
                                    $("#load").hide();
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    console.log(textStatus, errorThrown);
                                }
                            });
                        }
                        
                        $('#loadingmessage').hide();
                        if (circleclick != '') {
                            $("#w0").submit();
                    }
                    }

                    $("#p0").on('pjax:success', function (res, data) { 
                        if (mycircle == '') {
                            deleteSelectedShape();

                            selectedrouts('', true, 'getparam');
                        }
                        $.pjax.reload({url: "index",
                            method: 'POST', container: '#p1', data: $('#w0').serializeArray()});
                        
                        $('.select2data').select2();
                    });
                    $("#p1").on('pjax:beforeSend', function (xhr, options, settings) {
                    });

                    $(document).on('pjax:success', function (data, status, xhr, options) {
                        if (allcheck) {
                            $('input:checkbox').prop('checked', true);
                        } else {
                            $.each(allrouts, function (index, value) {
                                if ($('#srouts' + value).length) {
                                    $('#srouts' + value).prop('checked', true);
                                }
                                ;
                            });
                        }
                    });

                    function setDrawingTool() {


                        var drawingManager = new google.maps.drawing.DrawingManager({
                            // drawingMode: google.maps.drawing.OverlayType.MARKER,
                            //drawingControl: true,
                            drawingControlOptions: {
                                position: google.maps.ControlPosition.TOP_CENTER,
                                drawingModes: ['circle']
                            },
                            circleOptions: {
                                fillColor: '#ffff00',
                                // fillOpacity: 1,
                                strokeWeight: 5,
                                clickable: false,
                                editable: true,
                                zIndex: 1
                            }
                        });
                        drawingManager.setMap(map);
                        drawArray = drawingManager;
                        google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {
                            if (event.type == google.maps.drawing.OverlayType.CIRCLE) {
                                drawingManager.set('drawingMode');
                                // selectedrouts('initial load', true);
                                // deleteSelectedShape();
                                var radius = event.overlay.getRadius();
                                currentOverlay = event.overlay;
                                allrouts = [];
                                for (var i = 0; i < markers.length; i++) {
                                    if (event.overlay.getBounds().contains(markers[i].getPosition())) {
                                        var index = allrouts.indexOf(markers[i].id);
                                        if (index > -1) {
                                        } else {
                                            allrouts.push(markers[i].id);
                                        }
                                        // routeid += route + ",";
                                    }
                                }
                                if (allrouts.length == 0) {
                                    allrouts.push(-1);
                                }
                                $("#importantlocationsdetailssearch-ild_id").val(allrouts.join(","));
                                $("#w0").submit();


                                clearSelection();
                                var shape = event.overlay;
                                selectedShape = shape;
                                shape.setEditable(false);
                                mycircle = "circle";
                                selectedrouts('', false, '', 'circledata');
                                showRadius();

                            }
                        });



                        google.maps.event.addListener(drawingManager, 'drawingmode_changed', function (event) {
                            if (drawingManager.getDrawingMode() == 'circle') {
                                deleteSelectedShape();
                               // selectedrouts('', true, '', '', 'myclick');
                                if (infoWindow != null) {
                                    $("#map").mousemove(function (e) {
                                        infoWindow.setMap(null);
                                    });
                                }
                            }
                        });
                    }

                    function clearSelection() {
                        if (selectedShape) {
                            selectedShape.setEditable(false);
                            selectedShape = null;
                        }
                    }
                    function resetForm() {
                        mycircle = "";
                        $("#importantlocationsdetailssearch-ild_id").val('');
                        $("#w0").yiiActiveForm('resetForm');
                        if (infoWindow != null) {
                            $("#map").mousemove(function (e) {
                                infoWindow.setMap(null);
                            });
                        }
                    }

                    function exportdata() {
                        //var type = JSON.stringify($('#w0').serializeArray());
                        // window.location = "<?= Yii::$app->urlManager->createUrl('offlineroutes/export?type='); ?>" + type;
                        $.ajax({
                            url: "<?= Yii::$app->urlManager->createUrl('importantlocationsdetails/exportpost'); ?>",
                            type: "post",
                            data: $('#w0').serializeArray(),
                            success: function (response) {
                                // you will get response from your php page (what you echo or print)     
                                var downloadLink;
                                var dataType = 'application/vnd.ms-excel';
                                // $("#tblData").html(response);
                                var tableSelect = response;
                                var tableHTML = response;
                                //var tableHTML = response.replace(/ /g, '%20');
                                // Specify file name
                                filename = 'importantlocationsdetails.xls';

                                // Create download link element
                                downloadLink = document.createElement("a");

                                document.body.appendChild(downloadLink);

                                var blob = new Blob(['\ufeff', tableHTML], {
                                    type: dataType
                                });
                                if (navigator.msSaveOrOpenBlob) {

                                    navigator.msSaveOrOpenBlob(blob, filename);
                                } else {
                                    var url = URL.createObjectURL(blob);
                                    downloadLink.setAttribute("href", url);
                                    downloadLink.setAttribute("download", filename);
                                    downloadLink.style.visibility = 'hidden';
                                    document.body.appendChild(downloadLink);
                                    downloadLink.click();
                                    document.body.removeChild(downloadLink);

                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log(textStatus, errorThrown);
                            }


                        });

                    }

                    function clearOverlays() {

                        if (markers) {
                            for (i in markers) {
                                markers[i].setMap(null);
                            }
                        }

                        if (markerCluster) {
                            markerCluster.clearMarkers();
                        }

                    }
                    function deleteSelectedShape() {
                        if (selectedShape) {
                            selectedShape.setMap(null);
                        }
                        mycircle = "";
                    }
                    function setvalue() {
                        mycircle = "";
                    }

                    function showRadius() {
                        if (infoWindow != null) {
                            infoWindow.setMap(null);
                        }
                        $("#map").unbind("mousemove");
                        $("#distanceSign SPAN.radiusValue").text((currentOverlay.getRadius() / 1000.0).toFixed(3));
                        var content = $("#distanceSign").html();
                        infoWindow = new google.maps.InfoWindow({
                            content: content,
                            position: currentOverlay.getCenter()
                        });
                        infoWindow.open(map);

                    }
</script>
<script>
    function loadGoogleMaps() {
        var script_tag = document.createElement('script');
        script_tag.setAttribute("type", "text/javascript");
        var path = baseUrl + "resource/mapjs/oms.min.js";
        script_tag.setAttribute("src", path);
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
    }

    $(".myButton1").click(function () {
        $("#load").css("display", "block");
   

    });

//     document.onreadystatechange = function () {
//   var state = document.readyState
//   if (state == 'interactive') {
//        document.getElementById('contents').style.visibility="hidden";
//   } else if (state == 'complete') {
//       setTimeout(function(){
//          document.getElementById('interactive');
//          document.getElementById('load').style.visibility="hidden";
//          document.getElementById('contents').style.visibility="visible";
//       },500);
//   }
// }

//    $(document).ready(function () {
//        $("#load").css("display", "block");
//        setTimeout(function () {
//            $("#load").css("display", "none");
//        }, 10000);
//    });

</script>